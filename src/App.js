import React, {Component} from "react";
import "./App.css";
import {Popover, Button} from "@material-ui/core";
import logo from "./logo.svg";

// uncomment this
const DATA = [
    {
        id: 1,
        question:
            "Aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit1  ?",
        answer: "Yes",
    },
    {
        id: 2,
        question:
            "Lore aut odit aut fugit, sed quia ceos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum qui dolor sit ?",
        answer: "Yes",
    },
    {
        id: 3,
        question: "Lorem ipsum dolor sit amet, sum dolor sit amet, consectetuer ?",
        answer: "No",
    },
    {
        id: 4,
        question:
            "Bem ipsum dolor sit amet, consectetuer, sum dolor sit amet, lorem ipsum dolor sit amet, consectetuer ?",
        answer: "Yes",
    },
];


function App() {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>

                <div className="Regles">
                    À l'aide de React et Material-ui, construire un{" "}
                    <a href="https://material-ui.com/components/popover/" className="lien">
                        Popover
                    </a>{" "}
                    flexible et réutilisable
                </div>
                <ul className="rules">
                    <li>
                        Libre à vous du look final, mais un exemple de ce qui est attendu se trouve dans
                        /public/exemple.mp4
                    </li>
                    <li>Le code doit être clair et de vous</li>
                    <li>Le data qui doit être contenu dans le popover est dans la variable DATA</li>
                    <li>L'ajout de tests est fortement conseillé</li>
                </ul>
            </header>
            <section>
                <MyPopOver datas={DATA}/>
            </section>
        </div>
    );
}


class MyPopOver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            datasFromProps: null,
            isOpen: false,
            anchorEl: null,
        };
    }

    componentDidMount() {
        this.setState({
            datasFromProps: this.props.datas
        });
    }

    handleClick = (e) => {
        this.setState({
            isOpen: true,
            anchorEl: e.currentTarget
        });
    }
    handleClose = (e) => {
        this.setState({
            isOpen: false
        });
    }

    render() {
        const {isOpen, anchorEl} = this.state;
        const typos = this.state.datasFromProps && this.state.datasFromProps.map((val, ind, arr) => {
            return (
                <span className={"container"} key={val.id}>
                    <span className={"question"}>{val.question}</span>
                    <span className={"answer"}>{val.answer}</span>
                    {ind < arr.length - 1 ? <hr/> : null}
                </span>
            );
        });

        return (
            <div>
                <Button onClick={this.handleClick}>Exemple Greybox</Button>
                <Popover
                    open={isOpen}
                    onClose={this.handleClose}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}>
                    <p className={"header"}>
                        Daily Questions
                        <button className={'btnClose'} onClick={this.handleClose}>X</button>
                    </p>
                    {typos}
                </Popover>
            </div>
        )
    }
}


export default App;
